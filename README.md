# A bad Nim solver

Bad in the sense that it is slow: 1) although it does account for the
fact that [1, 1, 3] is equivalent to [3, 1, 1], it does not do anything
to avoid repeating work when the same state is reached by different paths
(see: memoization/dynamic programming); 2) it doesn't use the Nim xor
rule.

Command-line arguments are number of stones in each pile.

Output is (r, n) to indicate take n stones from a row currently containing r.
