import sys

''' Finds winning moves for Nim.  Note that this is *not* the best way
    to do this; this code serves as a demonstration of the basic property
    of a winning position and the need for dynamic programming.
'''

def successors(p):
    ''' Finds all the possible positions reachable in one move from
        the given position.  Returns a list of positions such that
        any possible move results in a position that is equivalent
        to one on the list.
    
        p -- a sorted list of positive integers
    '''
    # the list of possible re
    result = []
    for move_group in range(0, len(p)):
        if move_group == 0 or p[move_group] != p[move_group - 1]:
            # first group of this size
            group_size = p[move_group]
            for take in range(1, group_size + 1):
                remaining = group_size - take
                if remaining > 0:
                    succ = p[:move_group] + [remaining] + p[move_group + 1:]
                    result.append(((group_size, take), sorted(succ)))
                else:
                    result.append(((group_size, take),
                                   p[:move_group] + p[move_group + 1:]))
    return result

def winning_move(p):
    ''' Finds and returns a winning move for the given position, or
        None if there is no winning move.

        p -- an increasing list of positive integers
    '''
    # find the first move for which the resulting position is a losing position
    # (does not have a winning position)
    win = next((move for move, succ in successors(p) if winning_move(succ) is None), None)
    return win

def usage():
    print("USAGE: %s sticks-in-row..." % sys.argv[0])

if __name__ == "__main__":
    try:
        state = [int(x) for x in sys.argv[1:]]
        if len([x for x in state if x < 0]) > 0:
            raise ValueError("numbers of sticks must be nonnegative")
        print(winning_move(sorted([x for x in state if x > 0])))
    except:
        usage()
